const express = require("express");
const bodyParser = require('body-parser')
const questionsRouter = require("./routers/questions");
const cors = require('cors');
const app = express()
var http = require('http').createServer(app);

const port = process.env.PORT || "8000";



//  App Configuration
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());


// Routes Definitions
app.use("/api/questions", questionsRouter);


// Server Activation
http.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});