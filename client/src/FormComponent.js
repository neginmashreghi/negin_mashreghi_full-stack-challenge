import React, { Component, Fragment } from "react";
import { Button } from 'antd';
import './Form.css'
const axios = require('axios');
export default class FormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      question:"",
      displaySend: false
    };
  }
 
  generateQuestion =()=>{
    axios.get("http://localhost:8000/api/questions/get")
    .then( (response) => {
     
      this.setState({
        question: response.data.questions,
        displaySend: true
      })
    })
    .catch( (error)=>{
      console.log(error);
    }); 
  }
  sendEmail=()=>{

  const URL = "https://maker.ifttt.com/trigger/send_email/with/key/{your_key}"
  const body= {
    value1: "programs@prepr.org",
      value2: "Negin Mashreghi",
      value3: this.state.question
  }
 
     axios.post(URL,body)
    .then( (response) => {
      console.log(response);
    })
    .catch( (error)=>{
      console.log(error);
    
    })  
  }
  
    render() {
      const {question , displaySend}=this.state
        return (
            <Fragment>
        <div className="main_page">
        
          <div className="form_wrapper">
    
            <div className="form">
              <Button type="primary" htmlType="submit" onClick={this.generateQuestion} className ="generate">
                Generate Question
              </Button>
          
              <h2>{question}</h2>
              {displaySend?(
                <Button type="primary" htmlType="submit" onClick={this.sendEmail} className ="send">
                  Send Email
                </Button>

              ):(null)}
              
            </div>
        
        </div>

        </div>
      </Fragment>
        )
    }
}
