## Challenge
Write a script (programming language of your choice) to scrape the 61 questions from this page. Next, save them to a .csv file. Choose one of the questions randomly and use IFTTT webhook to email that question to “programs@prepr.org”.  Please provide the CSV output and a link to your source code (Github or similar). 

 

### Built With

Server is build on a Node/Express/React stack

- [NodeJS](https://nodejs.org/)
- [Express](https://expressjs.com/)
- [React](https://reactjs.org/)
- [IFTTT webhook](https://ifttt.com/maker_webhooks)


## Getting Started

To get a hot-loaded local instance up and running. The client folder contains a CRA (create-react-app) flavoured webpack setup. For development mode, the node package [concurrently](https://www.npmjs.com/package/concurrently) is used to run a separate instance of Express and CRA. 

### Prerequisites

You will need NodeJS/npm version 10+

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
nvm install 12.x.x
npm install npm@latest -g
```






### Installation
Open terminal make sure you are in **user_manager_postgres** folder then start installing the packages by run these commands:

```sh
npm install
npm run server-install
npm run client-install


```

### RUN
Run client on http://localhost:3000

```sh
npm run client

```
Run server on http://localhost:8000

```sh
npm run server

```

Run server and client 

```sh
npm run dev

```




